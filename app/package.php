<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class package extends Eloquent
{
    protected $connection = 'mongodb';

    protected $collection = 'package';
    protected $primaryKey = "_id";
    
    public function connote(){
        return $this->belongsTo(connote::class,'connote_id','_id');
    }
    protected $fillable = [
        'customer_name',
        'customer_code',
        'transaction_amount',
        'transaction_discount',
        'transaction_payment_type',
        'transaction_state',
        'transaction_code',
        'transaction_order',
        'location_id',
        'organization_id',
        'transaction_payment_type_name',
        'transaction_cash_amount',
        'transaction_cash_change',
        'customer_attribute',
        'connote',
        'connote_id',
        'destination_data',
        'koli_data',
        'custom_field',
        'currentLocation',
    ];
}
