<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class koli extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'kali';
    protected $primaryKey = "_id";

    
    protected $fillable = [
        'koli_length',
        'awb_url',
        'koli_chargeable_weight',
        'koli_width',
        'koli_surcharge',
        'koli_height',
        'koli_description',
        'koli_formula_id',
        'connote_id',
        'koli_volume',
        'koli_weight',
        'koli_id',
        'awb_sicepat' ,
        'harga_barang',
        'koli_code'
    ];
    
}
