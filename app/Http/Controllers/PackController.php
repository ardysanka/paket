<?php

namespace App\Http\Controllers;

use App\package;
use App\connote;
use App\koli;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Http\Response;

class PackController extends Controller
{
  
    public function index()
    {
        $data = package::with('connote')->get();
        return response($data);
    }


    public function create()
    {
       

    }

    
    public function store(Request $request)
    {
        $validator = Validator::make($request->input(),[
            'customer_name'=>'required|max:50',
            'customer_code'=>'required|max:7',
            'transaction_amount'=>'required|max:10',
            'transaction_discount'=>'max:10',
            'transaction_payment_type'=>'required|max:2',
            'transaction_state'=>'required|max:10',
            'transaction_code'=>'required|max:16',
            'transaction_order'=>'required|max:3',
            'location_id'=>'required|max:24',
            'organization_id'=>'required|max:1',
            'transaction_payment_type_name'=>'required|max:15',
            'transaction_cash_amount'=>'max:10',
            'transaction_cash_change'=>'max:10',
            'nama_sales'=>'required|max:50',
            'top'=>'required',
            'jenis_pelanggan'=>'required',

            'connote_number'=>'required',
            'connote_service'=>'required',
            'connote_code'=>'required',
            'connote_order'=>'required',
            'connote_state'=>'required',
            'connote_state_id'=>'required',
            'zone_code_from'=>'required',
            'zone_code_to'=>'required',
            'actual_weight'=>'required',
            'volume_weight'=>'required',
            'chargeable_weight'=>'required',
            'connote_total_package'=>'required',
            'connote_surcharge_amount'=>'required',
            'connote_sla_day'=>'required',
            'location_name'=>'required',
            'location_type'=>'required',
            'source_tariff_db'=>'required',
            'id_source_tariff'=>'required',

            "od_name"=>'required',
            "od_address"=>'required',
          
            "od_phone"=>'required',
            
            "od_zipcode"=> 'required',
            
            "dd_name"=>'required',
            "dd_address"=>'required',
            
            "dd_phone"=>'required',
            
            "dd_zipcode"=> 'required',

            "clname"=> 'required',
            "clcode"=> 'required',
            "cltype"=> 'required'
            


        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => '403',
                'message' => 'Error Validation',
             ], Response::HTTP_FORBIDDEN);
        }
        $transcode =  $request->input('zone_code_from').date("Ymd").$request->input('transaction_order');
        
        
        $data = package::create([
            'customer_name'=>$request->input('customer_name'),
            'customer_code'=>$request->input('customer_code'),
            'transaction_amount'=>$request->input('transaction_amount'),
            'transaction_discount'=>$request->input('transaction_discount'),
            'transaction_payment_type'=>$request->input( 'transaction_payment_type'),
            'transaction_state'=>$request->input('transaction_state'),
            'transaction_code'=>$transcode,
            'transaction_order'=>$request->input('transaction_order'),
            'location_id'=>$request->input('location_id'),
            'organization_id'=>$request->input('organization_id'),
            'transaction_payment_type_name'=>$request->input('transaction_payment_type_name'),
            'transaction_cash_amount'=>$request->input('transaction_cash_amount'),
            'transaction_cash_change'=>$request->input('transaction_cash_change'),
            'nama_sales'=>$request->input('nama_sales'),
            'top'=>$request->input('top'),
            'jenis_pelanggan'=>$request->input('jenis_pelanggan'),
        ]);
        $connote = connote::create([
            'connote_number'=>$request->input('connote_number'),
            'connote_service'=> $request->input('connote_service'),
            'connote_service_price'=> $request->input('transaction_amount'),
            'connote_amount'=> $request->input('transaction_amount'),
            'connote_code'=> 'AWB00100209082020',
            'connote_booking_code'=> '',
            'connote_order'=> $request->input('connote_order'),
            'connote_state'=> $request->input('connote_state'),
            'connote_state_id'=> $request->input('connote_state_id'),
            'zone_code_from'=> $request->input('zone_code_from'),
            'zone_code_to'=> $request->input('zone_code_to'),
            'surcharge_amount'=> null,
            'transaction_id'=>$data->_id ,
            'actual_weight'=> $request->input('actual_weight'),
            'volume_weight'=> $request->input('volume_weight'),
            'chargeable_weight'=>$request->input('chargeable_weight'),
            'organization_id'=> $request->input('organization_id'),
            'location_id'=> $request->input('location_id'),
            'connote_total_package'=>  $request->input('connote_total_package'),
            'connote_surcharge_amount'=> $request->input('connote_surcharge_amount'),
            'connote_sla_day'=> $request->input('connote_sla_day'),
            'location_name'=> $request->input('location_name'),
            'location_type'=> $request->input('location_type'),
            'source_tariff_db'=> $request->input('source_tariff_db'),
            'id_source_tariff'=>  $request->input('id_source_tariff'),
            'pod'=> null,
        ]);
        $package = package::where('_id',$data->_id)->first();
        $package->connote_id = $connote->_id;
     
        $origin_data=array([
            "customer_name"=>$request->input("od_name"),
            "customer_address"=>$request->input("od_address"),
            "customer_email"=>$request->input("od_email"),
            "customer_phone"=>$request->input("od_phone"),
            "customer_address_detail"=>$request->input("od_add_detail"),
            "customer_zip_code"=> $request->input("od_zipcode"),
            "zone_code"=> $request->input('zone_code_from'),
            "organization_id"=>$request->input('organization_id'),
            "location_id"=> $request->input('location_id'),
            
        ]);
        $destination_data=array([
            "customer_name"=> $request->input("dd_name"),
            "customer_address"=> $request->input("dd_address"),
            "customer_email"=> $request->input("dd_email"),
            "customer_phone"=> $request->input("dd_phone"),
            "customer_address_detail"=>$request->input("dd_add_detail"),
            "customer_zip_code"=> $request->input("dd_zipcode"),
            "zone_code"=>$request->input('zone_code_to'),
            "organization_id"=> $request->input('organization_id'),
            "location_id"=> $request->input('location_id'),
            
        ]);
        $current_location = array([
            "name"=> $request->input('clname'),
            "code"=>  $request->input('clcode'),
            "type"=>  $request->input('cltype')
        ]);
        $package->origin_data=$origin_data;
        $package->destination_data=$destination_data;
        $package->current_location=$current_location;
        $package->save();
        return response($data->_id);
    }

 
    public function show($id)
    {
        $data = package::with('connote')->find($id);
        return response($data);
        
    }

    
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->input(),[
            'koli_length'=>'required',
        
            'koli_chargeable_weight'=>'required',
            'koli_width'=> 'required',

            'koli_height'=>'required',
            'koli_description'=>'required',
            'koli_formula_id'=>'required',
     
            'koli_volume'=>'required',
            'koli_weight'=>'required',
          
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => '403',
                'message' => 'Error Validation',
             ], Response::HTTP_FORBIDDEN);
        }
        $data = package::with('connote')->find($id);
        $koli = koli::orderBy('_id','desc')->get();
        if(empty($koli)){
            $koli_code=$data->connote_code.".1";
        }else{
            $arr = explode(".", $koli->koli_code);
            $last = $arr[1]+1;
            $koli_code=$data->connote_code.".".$last;
        }
        $data = package::create([
            'connote_id'=>$data->connote_id,
            'koli_length'=>$request->index('koli_length'),
        ]);
        
    }

    
    public function destroy($id)
    {
        $data = package::find($id);
        $data2 = connote::find($data->_id);
        $data->delete();
        $data2->delete();
    }
}

