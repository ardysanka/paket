<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::GET('package','PackCOntroller@index');
Route::GET('package/{id}','PackCOntroller@show');
Route::post('package');
Route::PUT('package/{id}','PackCOntroller@edit');
Route::PATCH('package/{id}','PackCOntroller@update');
Route::delete('package/{id}','PackCOntroller@destroy');

