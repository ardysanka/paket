<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
            // $response = $this->get('/');
            $response = $this->get(route('api'),[
                'customer_name'=>$this->faker->name,
                'customer_code'=>$this->faker->randomNumber(7),
                'transaction_amount'=>$this->faker->randomNumber(10),
                'transaction_discount'=>$this->faker->randomNumber(10),
                'transaction_payment_type'=>$this->faker->randomNumber(2),
                'transaction_state'=>$this->faker->randomNumber(10),
                'transaction_code'=>$this->faker->randomNumber(10),
                'transaction_order'=>$this->faker->randomNumber(3),
                'location_id'=>$this->faker->randomNumber(24),
                'organization_id'=>$this->faker->randomNumber(1),
                'transaction_payment_type_name'=>$this->faker->randomNumber(5),
                'transaction_cash_amount'=>0,
                'transaction_cash_change'=>0,
                'nama_sales'=>$this->faker->randomNumber(50),
                'top'=>$this->faker->randomNumber(10),
                'jenis_pelanggan'=>$this->faker->randomNumber(3),
            ]);
            $response->assertStatus(200);
        $response->assertStatus(200);
    }
}
